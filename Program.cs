﻿using System;

namespace PatronObserverEvaluacion
{
    class Program
    {
        static void Main(string[] args)
        {

			// Implementacion del patron observer simula el cambio de dolares a determinadas divisas
			// Los observadores van a reaccionar para poder mostrar un mensaje - estado
			
			Subject subject = new Subject(); // Creamos una instancia del sujeto

			// Se crean instancias de los observadores pasandoles el sujeto
			new SolObservador(subject);
			new PesoARGObservador(subject);
			new PesoMXObservador(subject);

			// Mostramos por pantalla el cambio realizado
			Console.WriteLine("Si desea cambiar 10 dólares obtendrá : ");
			subject.setEstado(10);
			Console.WriteLine("-----------------");
			Console.WriteLine("Si desea cambiar 100 dólares obtendrá : ");
			subject.setEstado(100);
		}
    }
}
