﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObserverEvaluacion
{
	// Clase que implementa la interfaz observador
	public class PesoARGObservador : Observador
    {
		// Definimos un valor fijo 
		private double valorCambio = 29.86;

		public PesoARGObservador(Subject sujeto)
		{
			this.sujeto = sujeto;
			this.sujeto.agregar(this);
		}

		// Creamos una sobreescritura del metodo actualizar 
		public override void actualizar()
		{
			Console.WriteLine("ARG: " + (sujeto.getEstado() * valorCambio));
		}
	}
}
