﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObserverEvaluacion
{
    // Clase abstracta que es el observador
    // Contiene como atributo al sujeto en mencion y la funcion actualizar
    public abstract class Observador
    {
        protected Subject sujeto;
        public abstract void actualizar();
    }
}
