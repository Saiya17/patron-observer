﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObserverEvaluacion
{
	// Clase que implementa la interfaz observador
    public class SolObservador : Observador
    {
		// Definimos un valor fijo 
		private double valorCambio = 3.25;

		public SolObservador(Subject sujeto)
		{
			this.sujeto = sujeto;
			this.sujeto.agregar(this);
		}

	// Creamos una sobreescritura del metodo actualizar 
	public override void actualizar()
		{
			Console.WriteLine("PEN: " + (sujeto.getEstado() * valorCambio));
		}
	}
}
