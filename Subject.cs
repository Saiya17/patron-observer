﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObserverEvaluacion
{
    public class Subject
    {
		// Creamos una Lista Llamada Observadores
		private List<Observador> observadores = new List<Observador>();
		private int estado;

		// Cuando el estado es cambiado este notifica a todos los observadores
		public int getEstado()
		{
			return estado;
		}

		public void setEstado(int estado)
		{
			this.estado = estado;
			notificarTodosObservadores();
		}
		// Metodo agregar observadores
		public void agregar(Observador observador)
		{
			observadores.Add(observador);
		}
		// Metodo notificar observadores
		public void notificarTodosObservadores()
		{
			observadores.ForEach(x=>x.actualizar());
		}

	}
}
