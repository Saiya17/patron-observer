﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObserverEvaluacion
{
	// Clase que implementa la interfaz observador
	public class PesoMXObservador : Observador
    {
		// Definimos un valor fijo 
		private double valorCambio = 19.07;

		public PesoMXObservador(Subject sujeto)
		{
			this.sujeto = sujeto;
			this.sujeto.agregar(this);
		}

		// Creamos una sobreescritura del metodo actualizar 
		public override void actualizar()
		{
			Console.WriteLine("MX: " + (sujeto.getEstado() * valorCambio));
		}

	}
}
